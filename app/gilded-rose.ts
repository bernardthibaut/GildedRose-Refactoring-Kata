import { MAX_ITEM_QUALITY, MIN_ITEM_QUALITY } from './constants';

export class Item {
  name: string;
  sellIn: number;
  quality: number;

  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

export enum ItemType {
  Legendary = 'Legendary',
  BackstagePasses = 'BackstagePasses',
  AgedCheese = 'AgedCheese',
  Conjured = 'Conjured',
}

export class SpecialItem extends Item {
  type: ItemType;

  constructor(name: string, sellIn: number, quality: number, type: ItemType) {
    super(name, sellIn, quality);
    this.type = type;
  }
}

export class GildedRose {
  items: Array<Item>;

  constructor(items = [] as Array<Item>) {
    this.items = items;
  }

  updateQuality(): Item[] {
    return this.items.map((item) => this._updateItemQuality(item));
  }

  private _updateItemQuality(item: Item): Item {
    if (!(item instanceof SpecialItem)) {
      return this._updateSimpleItemQuality(item);
    }

    switch (item.type) {
      case ItemType.Legendary:
        return item;
      case ItemType.BackstagePasses:
        return this._updateBackstagePassesQuality(item);
      case ItemType.AgedCheese:
        return this._updateAgedCheeseQuality(item);
      case ItemType.Conjured:
        return this._updateConjuredQuality(item);
      default:
        return this._updateSimpleItemQuality(item);
    }
  }

  private _updateSimpleItemQuality(item: Item): Item {
    this._computeUpdateQuality(item, -1);
    this._computeUpdateSellIn(item);

    if (item.sellIn < 0) {
      this._computeUpdateQuality(item, -1);
    }

    return item;
  }

  private _updateBackstagePassesQuality(item: SpecialItem): SpecialItem {
    const qualityChange: number =
      item.sellIn < 6 ? 3 : item.sellIn < 11 ? 2 : 1;
    this._computeUpdateQuality(item, qualityChange);
    this._computeUpdateSellIn(item);

    if (item.sellIn < 0) {
      item.quality = MIN_ITEM_QUALITY;
    }

    return item;
  }

  private _updateAgedCheeseQuality(item: SpecialItem): SpecialItem {
    this._computeUpdateQuality(item, 1);
    this._computeUpdateSellIn(item);

    if (item.sellIn < 0) {
      this._computeUpdateQuality(item, 1);
    }

    return item;
  }

  private _updateConjuredQuality(item: SpecialItem): SpecialItem {
    this._computeUpdateQuality(item, -2);
    this._computeUpdateSellIn(item);

    if (item.sellIn < 0) {
      this._computeUpdateQuality(item, -2);
    }

    return item;
  }

  private _computeUpdateQuality(item: Item, qualityChange: number): Item {
    const newQuality: number = item.quality + qualityChange;

    if (qualityChange < 0 && item.quality > MIN_ITEM_QUALITY) {
      item.quality =
        newQuality > MIN_ITEM_QUALITY ? newQuality : MIN_ITEM_QUALITY;
    } else if (qualityChange > 0 && item.quality < MAX_ITEM_QUALITY) {
      item.quality =
        newQuality < MAX_ITEM_QUALITY ? newQuality : MAX_ITEM_QUALITY;
    }

    return item;
  }

  private _computeUpdateSellIn(item: Item): Item {
    item.sellIn--;
    return item;
  }
}
