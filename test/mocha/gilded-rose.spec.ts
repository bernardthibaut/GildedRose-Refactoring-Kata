import { GildedRose, Item, ItemType, SpecialItem } from '@/gilded-rose';
import { expect } from 'chai';

describe('Gilded Rose', () => {
  describe('Simple items', () => {
    it('should add a new item to the list', () => {
      const gildedRose = new GildedRose([new Item('foo', 0, 0)]);
      const [foo] = gildedRose.items;
      expect(foo.name).to.equal('foo');
      expect(foo.sellIn).to.equal(0);
      expect(foo.quality).to.equal(0);
    });

    it('should add multiple new items to the list', () => {
      const gildedRose = new GildedRose([
        new Item('foo', 0, 0),
        new Item('bar', 10, 20),
      ]);
      const [foo, bar] = gildedRose.items;
      expect(foo.name).to.equal('foo');
      expect(foo.sellIn).to.equal(0);
      expect(foo.quality).to.equal(0);
      expect(bar.name).to.equal('bar');
      expect(bar.sellIn).to.equal(10);
      expect(bar.quality).to.equal(20);
    });

    it('should decrement the quality when updating', () => {
      const gildedRose = new GildedRose([new Item('foo', 10, 20)]);
      gildedRose.updateQuality();
      const [foo] = gildedRose.items;
      expect(foo.quality).to.equal(19);
    });

    it('should decrement the sell by date when updating', () => {
      const gildedRose = new GildedRose([
        new Item('foo', 0, 0),
        new Item('bar', 10, 20),
      ]);
      gildedRose.updateQuality();
      const [foo, bar] = gildedRose.items;
      expect(foo.sellIn).to.equal(-1);
      expect(bar.sellIn).to.equal(9);
    });

    it('should decrement the quality twice as fast once the sell by date has passed', () => {
      const gildedRose = new GildedRose([
        new Item('foo', 0, 20),
        new Item('bar', -10, 20),
      ]);
      gildedRose.updateQuality();
      const [foo, bar] = gildedRose.items;
      expect(foo.quality).to.equal(18);
      expect(bar.quality).to.equal(18);
    });

    it('quality should never be negative', () => {
      const gildedRose = new GildedRose([
        new Item('foo', 0, 0),
        new Item('bar', 10, 0),
        new Item('abc', 0, 0),
        new Item('xyz', 0, 1),
      ]);
      gildedRose.updateQuality();
      const [foo, bar, abc, xyz] = gildedRose.items;
      expect(foo.quality).to.equal(0);
      expect(bar.quality).to.equal(0);
      expect(abc.quality).to.equal(0);
      expect(xyz.quality).to.equal(0);
    });
  });

  describe('Aged Brie', () => {
    it('should decrement the sell by date when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Aged Brie', 0, 0, ItemType.AgedCheese),
        new SpecialItem('Aged Brie', 10, 20, ItemType.AgedCheese),
      ]);
      gildedRose.updateQuality();
      const [agedBrie1, agedBrie2] = gildedRose.items;
      expect(agedBrie1.sellIn).to.equal(-1);
      expect(agedBrie2.sellIn).to.equal(9);
    });

    it('should increment the quality when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Aged Brie', 10, 20, ItemType.AgedCheese),
      ]);
      gildedRose.updateQuality();
      const [agedBrie] = gildedRose.items;
      expect(agedBrie.quality).to.equal(21);
    });

    it('should increment the quality twice as fast once the sell by date has passed', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Aged Brie', 0, 20, ItemType.AgedCheese),
        new SpecialItem('Aged Brie', -10, 20, ItemType.AgedCheese),
      ]);
      gildedRose.updateQuality();
      const [agedBrie1, agedBrie2] = gildedRose.items;
      expect(agedBrie1.quality).to.equal(22);
      expect(agedBrie2.quality).to.equal(22);
    });

    it('should not increment the quality above 50', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Aged Brie', 10, 49, ItemType.AgedCheese),
        new SpecialItem('Aged Brie', 0, 49, ItemType.AgedCheese),
      ]);
      const [agedBrie1, agedBrie2] = gildedRose.items;
      gildedRose.updateQuality();
      expect(agedBrie1.quality).to.equal(50);
      expect(agedBrie2.quality).to.equal(50);
      gildedRose.updateQuality();
      expect(agedBrie1.quality).to.equal(50);
      expect(agedBrie2.quality).to.equal(50);
    });
  });

  describe('Sulfuras', () => {
    it('should add sulfuras to the list', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Sulfuras, Hand of Ragnaros',
          0,
          80,
          ItemType.Legendary
        ),
      ]);
      const [sulfuras] = gildedRose.items;
      expect(sulfuras.name).to.equal('Sulfuras, Hand of Ragnaros');
      expect(sulfuras.sellIn).to.equal(0);
      expect(sulfuras.quality).to.equal(80);
    });

    it('should not decrement the sell by date when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Sulfuras, Hand of Ragnaros',
          0,
          80,
          ItemType.Legendary
        ),
      ]);
      gildedRose.updateQuality();
      const [sulfuras] = gildedRose.items;
      expect(sulfuras.sellIn).to.equal(0);
    });

    it('should not change the quality when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Sulfuras, Hand of Ragnaros',
          0,
          80,
          ItemType.Legendary
        ),
      ]);
      gildedRose.updateQuality();
      const [sulfuras] = gildedRose.items;
      expect(sulfuras.quality).to.equal(80);
    });
  });

  describe('Backstage passes', () => {
    it('should decrement the sell by date when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          0,
          0,
          ItemType.BackstagePasses
        ),
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          10,
          20,
          ItemType.BackstagePasses
        ),
      ]);
      gildedRose.updateQuality();
      const [backstagePasses1, backstagePasses2] = gildedRose.items;
      expect(backstagePasses1.sellIn).to.equal(-1);
      expect(backstagePasses2.sellIn).to.equal(9);
    });

    it('should increment the quality by 1 when updating and sellIn > 10', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          11,
          0,
          ItemType.BackstagePasses
        ),
      ]);
      gildedRose.updateQuality();
      const [backstagePasses] = gildedRose.items;
      expect(backstagePasses.quality).to.equal(1);
    });

    it('should increment the quality by 2 when updating and sellIn <= 10', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          10,
          0,
          ItemType.BackstagePasses
        ),
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          6,
          0,
          ItemType.BackstagePasses
        ),
      ]);
      gildedRose.updateQuality();
      const [backstagePasses1, backstagePasses2] = gildedRose.items;
      expect(backstagePasses1.quality).to.equal(2);
      expect(backstagePasses2.quality).to.equal(2);
    });

    it('should increment the quality by 3 when updating and sellIn <= 5', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          5,
          0,
          ItemType.BackstagePasses
        ),
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          1,
          0,
          ItemType.BackstagePasses
        ),
      ]);
      gildedRose.updateQuality();
      const [backstagePasses1, backstagePasses2] = gildedRose.items;
      expect(backstagePasses1.quality).to.equal(3);
      expect(backstagePasses2.quality).to.equal(3);
    });

    it('should drop the quality to 0 when updating and sellIn <= 0', () => {
      const gildedRose = new GildedRose([
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          0,
          20,
          ItemType.BackstagePasses
        ),
        new SpecialItem(
          'Backstage passes to a TAFKAL80ETC concert',
          -1,
          20,
          ItemType.BackstagePasses
        ),
      ]);
      gildedRose.updateQuality();
      const [backstagePasses1, backstagePasses2] = gildedRose.items;
      expect(backstagePasses1.quality).to.equal(0);
      expect(backstagePasses2.quality).to.equal(0);
    });
  });

  describe('Conjured items', () => {
    it('should decrement the sell by date when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Conjured', 0, 0, ItemType.Conjured),
        new SpecialItem('Conjured', 10, 20, ItemType.Conjured),
      ]);
      gildedRose.updateQuality();
      const [conjured1, conjured2] = gildedRose.items;
      expect(conjured1.sellIn).to.equal(-1);
      expect(conjured2.sellIn).to.equal(9);
    });

    it('should decrement the quality twice as fast as normal items when updating', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Conjured', 10, 20, ItemType.Conjured),
      ]);
      gildedRose.updateQuality();
      const [conjured] = gildedRose.items;
      expect(conjured.quality).to.equal(18);
    });

    it('should decrement the quality twice as fast once the sell by date has passed', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Conjured', 0, 20, ItemType.Conjured),
        new SpecialItem('Conjured', -10, 20, ItemType.Conjured),
      ]);
      gildedRose.updateQuality();
      const [conjured1, conjured2] = gildedRose.items;
      expect(conjured1.quality).to.equal(16);
      expect(conjured2.quality).to.equal(16);
    });

    it('quality should never be negative', () => {
      const gildedRose = new GildedRose([
        new SpecialItem('Conjured', 0, 0, ItemType.Conjured),
        new SpecialItem('Conjured', 10, 0, ItemType.Conjured),
        new SpecialItem('Conjured', 0, 0, ItemType.Conjured),
        new SpecialItem('Conjured', 0, 1, ItemType.Conjured),
      ]);
      gildedRose.updateQuality();
      const [foo, bar, abc, xyz] = gildedRose.items;
      expect(foo.quality).to.equal(0);
      expect(bar.quality).to.equal(0);
      expect(abc.quality).to.equal(0);
      expect(xyz.quality).to.equal(0);
    });
  });
});
