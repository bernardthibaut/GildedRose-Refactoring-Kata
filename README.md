# Gilded Rose

This is the Gilded Rose kata in TypeScript.

## Getting started

Install dependencies

```sh
npm install
```

## Run the unit tests from the Command-Line

Run the tests using Mocha

```sh
npm run test:mocha
```

## Contributing

### Commits

Your commit messages should follow the [Conventional Commits specification](https://www.conventionalcommits.org/en/v1.0.0/), which means that the commit message should be structured as follows:

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

The type must be one of the following:

- **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci**: Changes to our CI configuration files and scripts (examples: CircleCi, SauceLabs)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests

In this repository, the scope could be the (fictional) name of the ticket associated to the commit you are creating.

You can get more details about that convention on the [Conventional Commits website](https://www.conventionalcommits.org/en/v1.0.0/) or in the [CONTRIBUTING.md file](https://github.com/angular/angular/blob/main/CONTRIBUTING.md#commit) in the Angular project, which also uses that convention.
